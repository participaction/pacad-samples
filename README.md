**participaction**

# PACAd Sample Banners

This repository contains banner samples for the PACAd system.

## Installation

Clone this reposiory and install the dependencies

```bash
git clone https://bitbucket.org/participaction/pcad-samples .

npm install
```


## Running

To run the samples, please run the mini server from the command line. Then set your browser to the `127.0.0.1:8080`

```bash
npm run server
```


### Changing Samples

To change the template, point the bottom input field to the path of the sample you'd like to see.  i.e. if you want to see the **template** example, enter `template/index.html` and press `enter` or click LOAD. By default it points to the template unit.

> you can run your own samples or tests too!  simply point the field to the location of your entrypoint file.


## Specifications

For PACAd specification, please [visit the Wiki](https://bitbucket.org/participaction/pacad-samples/wiki/Home).
