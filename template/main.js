window.onload = function() {

    var clickTag = "https://www.participaction.com";
    var clickArea = document.getElementById('banner');
    var client = new PACAd('template_unit');
    client.clickTag(clickArea, clickTag);

    // ANIMATION

    var tl = new TimelineLite({});

    tl.to(bgImage,3,{y:-100, ease:Strong.easeOut}, 0)
    .to(bgImage,1,{opacity:1, ease:Strong.easeOut}, 0)
    .to(copy1, 3,{opacity:1}, "-=2")
    .to(copy1, 0.5,{opacity:0}, "+=0.5")
    .to(copy2, 0.5,{opacity:1}, "+=0")
    .to(copy2, 0.5,{opacity:0}, "+=2.5")
    .to(copy3, 0.5,{opacity:1}, "+=0")
    .to(cta, 0.5,{y:0,opacity:1}, "+=0")
    .from(bgImage, 10,{scale:1.3}, 0)

    // ANIMATION end

    client.on('loaded', function(event) {
        console.log('[PACAd:Event] Basic Unit Loaded', client.name);

        // Restarts animation incase it has been playing while the unit was loading, this allows the developer to view the banner in and out of the PACAD platform //
        tl.restart();
    });

};
